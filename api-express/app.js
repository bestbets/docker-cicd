const express = require('express');
const app = express();
const cors = require('cors');

app.use(cors());
app.get('/', (req, res) => res.json({ message: 'This is a message for you, Rudy!' }));

module.exports = app;