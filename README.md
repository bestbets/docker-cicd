# Microservices in Gitlab CI/CD

A demo can be seen at [https://ui.test-cicd.upchunk.com/](ui.test-cicd.upchunk.com)

## What is this?
This project is a demonstration of using GitLab's CI/CD to deploy multiple container to a VPS.

It features two containers:
* The UI is a customized demo of create-react-app, which shows a message from Star Command
* The API is hard-coded to send a JSON `{message: 'This is a message for you, Rudy!'}` which is displayed in the React UI.

When code in the repo is committed, a build and deployment will result.

![pipeline results](https://imgur.com/EwXaiuy)